/* global define */
define([
    './AbstractModule'
], function (
    AbstractModule
) {

    /**
     * @class
     * @extends AbstractModule
     */
    class Order extends AbstractModule {

        /**
         *
         * @param {!HTMLInputElement} field поле ввода
         * @param {!string} value значение
         */
        setField(field, value) {
            field.value = value;
            field.parentNode.classList.toggle('__small-placeholder', !!value.trim());
        }

        activate(element) {
            element.addEventListener('click', this);
            this.vendor = element.getAttribute('data-vendor') || "";
            this.article = element.getAttribute('data-article') || "";

            this.modal = document.querySelector('.js-modal');
            this.vendorField = document.querySelector('.js-text[name="vendor"]');
            this.articleField = document.querySelector('.js-text[name="article"]');
        }

        handleEvent(e) {
            if (e.type === 'click') {
                this.setField(this.vendorField, this.vendor);
                this.setField(this.articleField, this.article);
                this.vendorField.value = this.vendor;
                this.articleField.value = this.article;

                document.body.classList.add('__modal');
                this.modal.classList.add('__active');
                this.modal.focus();
                e.preventDefault();
            }
        }
    }

    return Order;
});
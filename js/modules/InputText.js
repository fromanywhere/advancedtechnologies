/* global define */
define([
    './AbstractModule'
], function (
    AbstractModule
) {

    class InputText extends AbstractModule {

        handleEvent(e) {
            if (e.type === 'focus' || e.type === 'blur') {
                this.toggleLabel();
            }
        }

        activate(element) {
            this.element = element;
            this.input = this.element.querySelector('.js-text');
            this.showLabel = true;

            if (this.input.value) {
                this.toggleLabel(true);
            }
            this.element.classList.add('__js-placeholder');
            this.input.removeAttribute('placeholder');

            this.input.addEventListener('focus', this);
            this.input.addEventListener('blur', this);
        }

        /**
         *
         * @param {boolean} forceHideLabel принудительно уменьшить лейбл
         */
        toggleLabel(forceHideLabel) {
            if (forceHideLabel === true) {
                this.showLabel = false;
            } else {
                if (this.input.value) {
                    this.showLabel = false;
                } else {
                    if (forceHideLabel === false) {
                        this.showLabel = true;
                    } else {
                        this.showLabel = document.activeElement !== this.input;
                    }
                }
            }

            this.showLabel
                ? this.element.classList.remove('__small-placeholder')
                : this.element.classList.add('__small-placeholder');
        }
    }

    return InputText;
});
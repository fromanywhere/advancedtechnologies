/* global define */
define([
    './AbstractModule',
    '../Utils.js'
], function (
    AbstractModule,
    utils
) {

    /**
     * @class
     * @extends AbstractModule
     */
    class ToggleHeight extends AbstractModule {

        activate(element) {
            this.element = element;
            this.isOpen = utils.isDesktop();
            this.handler = this.element.querySelector('.js-toggle-handle');
            this.body = this.element.querySelector('.js-toggle-body');

            this.setHeight();
            this.handler.addEventListener('click', this);
        }

        detectHeight() {
            var copy = this.body.cloneNode(true);
            copy.setAttribute('style', 'display: block !important; position: absolute; left: -9999px');
            document.body.appendChild(copy);
            var height = copy.clientHeight;
            document.body.removeChild(copy);
            return height;
        }

        setHeight() {
            this.body.style.maxHeight = this.isOpen
                ? this.detectHeight() + 'px'
                : '0px';
        }

        handleEvent(e) {
            if (e.type === 'click') {
                this.isOpen = !this.isOpen;
                this.setHeight();
            }
        }
    }

    return ToggleHeight;
});
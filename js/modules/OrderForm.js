/* global define */
define([
    './AbstractModule'
], function (
    AbstractModule
) {

    /**
     * @class
     * @extends AbstractModule
     */
    class OrderForm extends AbstractModule {

        activate(element) {
            this.element = element;
            this.close = this.element.querySelector('.js-close');
            this.agreeBox = this.element.querySelector('[name="agree"]');
            this.form = this.element.querySelector('form');
            this.successResult = this.element.querySelector('.order-popup_status.__success');
            this.errorResult = this.element.querySelector('.order-popup_status.__error');
            this.isVisible = this.element.getAttribute('data-visible') === 'true';

            this.close.addEventListener('click', this);
            this.form.addEventListener('submit', this);
            this.element.addEventListener('keydown', this);
        }

        clearStatus() {
            this.element.classList.remove('__success', '__error');
        }

        urlencodeFormData(formElements) {
            var s = '';
            function encode(text) {
                return encodeURIComponent(text).replace(/%20/g, '+');
            }

            for (var i=0; i < formElements.length; i++) {
                s += (s?'&':'') + encode(formElements[i].name)+'='+encode(formElements[i].value);
            }

            return s;
        }

        handleEvent(e) {
            switch (e.type) {
                case 'click':
                    if (!this.isVisible) {
                        document.body.classList.remove('__modal');
                        this.element.classList.remove('__active');
                        this.clearStatus();
                        e.preventDefault();
                    }
                    break;
                case 'submit':
                    e.preventDefault();
                    this.clearStatus();

                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", this.form.getAttribute('action'));
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    xhr.onload = function () {
                        try {
                            var response = JSON.parse(xhr.responseText);

                            if (response.status === 'error') {
                                this.errorResult.innerHTML = response.text;
                                this.element.classList.add('__error');
                            } else {
                                this.successResult.innerHTML = response.text;
                                this.element.classList.add('__success');
                            }
                        } catch(err) {
                            // noop
                        }
                    }.bind(this);
                    xhr.send(this.urlencodeFormData(this.form.elements));

                    break;
                case 'keydown':
                    if (!this.isVisible && (e.key === 'Escape')) {
                        document.body.classList.remove('__modal');
                        this.element.classList.remove('__active');
                        this.clearStatus();
                        e.preventDefault();
                    }
                    break;
            }
        }
    }

    return OrderForm;
});
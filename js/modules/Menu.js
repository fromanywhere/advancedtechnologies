/* global define */
define([
    '../Utils',
    './AbstractModule'
], function (
    utils,
    AbstractModule
) {

    /**
     * @class
     * @extends AbstractModule
     */
    class Menu extends AbstractModule {

        toggleSidebar() {
            document.body.classList.toggle('__modal', this.isSidebar);
            this.element.classList.toggle('__sidebar', this.isSidebar);
        }

        checkFixed() {
            if (window.pageYOffset >= this.breakpoint && !this.isFixed) {
                this.isFixed = true;
            }
            if (window.pageYOffset < this.breakpoint && this.isFixed) {
                this.isFixed = false;
            }
            this.element.classList.toggle('__fixed', this.isFixed);
        }

        checkDark() {
            if (this.isFixed) {
                this.element.classList.add('__dark');
            } else {
                if (!this.nextSibling.classList.contains('cover') && this.element.clientHeight > this.nextSibling.offsetTop) {
                    this.element.classList.add('__dark');
                } else {
                    this.element.classList.remove('__dark');
                }
            }
        }

        toggleMenu() {
            if (!utils.isDesktop()) {
                this.dropdownToggler.classList.toggle('__hover');
            }
        }

        overMenu() {
            if (utils.isDesktop()) {
                this.dropdownToggler.classList.add('__hover');
            }
        }

        leaveMenu(e) {
            if (utils.isDesktop()) {
                this.dropdownToggler.classList.remove('__hover');
            }
        }

        /**
         * Это специальный костыль, связанный с ошибкой расчета первоначального размера геометрии верхнего меню
         * Ошибка предположительно связана с отложенной подгрузкой кастомных шрифтов и проявляется не всегда
         * Ошибке подвержено как вычисление через offsetTop, так и через getBoundingClientRect().
         */
        geometryChangeCheck() {
            if (!this.isFixed) {
                var testBreakPoint = this.getBreakpoint();
                if (this.breakpoint !== testBreakPoint)  {
                    this.breakpoint = testBreakPoint;
                }
            }
        }

        getBreakpoint() {
            return 90;
        }

        scrollHandler() {
            this.geometryChangeCheck();
            this.checkFixed();
            this.checkDark();
        }

        resizeHandler() {
            if (utils.isDesktop()) {
                this.isSidebar = false;
                this.toggleSidebar();
                this.dropdownToggler.classList.remove('__hover');
            }
            this.geometryChangeCheck();
            this.checkFixed();
            this.checkDark();
        }



        activate(element) {
            this.element = element;
            this.menu = this.element.querySelector('.top-line_nav-menu');
            this.icon = this.element.querySelector('.top-line_nav-icon');
            this.right = this.element.querySelector('.top-line_right');
            this.dropdownToggler = this.element.querySelector('.nav-menu_item.__has-dropdown');
            this.nextSibling = this.element.nextElementSibling;
            this.breakpoint = this.getBreakpoint();
            this.isFixed = false;
            this.isSidebar = false;

            this.boundResizeHandler = this.resizeHandler.bind(this);
            this.boundScrollHandler = this.resizeHandler.bind(this);

            this.geometryChangeCheck();
            this.checkFixed();
            this.checkDark();

            window.addEventListener('scroll', utils.throttle(this.boundScrollHandler, 30), false);
            window.addEventListener('resize', utils.throttle(this.boundResizeHandler, 30), false);

            window.addEventListener('load', function () {
                this.geometryChangeCheck();
                this.checkDark();
            }.bind(this));

            this.element.addEventListener('click', function () {
                if (!utils.isDesktop()) {
                    this.isSidebar = false;
                    this.toggleSidebar();
                }
            }.bind(this));

            this.icon.addEventListener('click', function (e) {
                e.stopPropagation();
                this.isSidebar = !this.isSidebar;
                this.toggleSidebar();
            }.bind(this));

            this.right.addEventListener('click', function (e) {
                e.stopPropagation();
            });

            this.dropdownToggler.addEventListener('click', this.toggleMenu.bind(this), false);
            this.dropdownToggler.addEventListener('mouseover', this.overMenu.bind(this), false);
            this.dropdownToggler.addEventListener('mouseleave', this.leaveMenu.bind(this), false);
        }
    }

    return Menu;
});
/* global require, console */

const gulp = require('gulp');
const del = require('del');
const less = require('gulp-less');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const webpack = require('webpack-stream');
const eslint = require('gulp-eslint');
const babel = require("gulp-babel");
const path = require("path");
const uglify = require('gulp-uglify');
const harp = require('harp');
const copy = require('gulp-copy');
const runSequence = require('run-sequence');
const svgSprite = require("gulp-svg-sprites");
const csso = require('gulp-csso');

gulp.task('clean', function () {
    return del([
        'build',
        'css/style.css',
        'js/App.js'
    ]);
});

gulp.task('sprites', function () {
    const config = {
        baseSize: 10,
        preview: false,
        cssFile: 'css/_sprite.less',
        svg: {
            sprite: 'img/sprite.svg'
        },
        templates: {
            css: require("fs").readFileSync("./css/_sprite-template.css", "utf-8")
        }
    };

    return gulp.src('img/sprites/*.svg')
        .pipe(svgSprite(config))
        .pipe(gulp.dest("./"));
});

gulp.task('css', ['sprites'], function () {
    return gulp
        .src([
            'css/style.less'
        ])
        .pipe(less())
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: [
                'Explorer >= 10',
                'iOS >= 3',
                'Android >= 2',
                'Firefox ESR',
                'last 5 versions'
            ],
            cascade: false
        }))
        .pipe(csso())
        .pipe(gulp.dest('build/css'));
});

gulp.task('eslint', function () {
    return gulp
        .src([
            'js/**/*.js',
            '!js/modules/swiper.min.js',
            '!js/App.js'
        ])
        .pipe(eslint())
        .pipe(eslint.result(result => {
            // Called for each ESLint result.
            console.log(`### ESLint check: ${path.basename(result.filePath)} is ${result.messages.length ? 'FAIL' : 'OK'} ###`);
            if (result.messages.length) {
                result.messages.forEach((message) => {
                    console.log(message.message);
                    console.log(`Line ${message.line}, column ${message.column}`);
                })
            }
        })
        )
});

gulp.task('webpack', function() {
    return gulp
        .src('js/Entry.js')
        .pipe(webpack(Object.assign({
            output: {
                filename: 'App.js'
            }
        })))
        .pipe(babel())
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('watch-webpack', function() {
    return gulp
        .src('js/Entry.js')
        .pipe(webpack(Object.assign({
            watch: true,
            output: {
                filename: 'App.js'
            }
        })))
        .pipe(babel())
        .pipe(gulp.dest('js'));
});

gulp.task('watch', ['sprites', 'eslint'], function() {
    harp.server(__dirname, {
        port: 9000
    }, function () {
        console.log('Server running on http://localhost:9000/');
        gulp.start('watch-webpack');
    });
});

gulp.task('copy', function() {
    return gulp
        .src([
            '!node_modules/**/*.*',
            './**/*.ejs',
            './fonts/*.*',
            './img/**/*.*',
            './js/App.js',
            './_data.json',
            './_harp.json',
            './favicon.png'
        ])
        .pipe(copy('build'));
});

gulp.task('default', function () {
    runSequence('clean', 'eslint', 'webpack', 'copy', 'css', function() {
        harp.compile('./build', '../www', function () {
            console.log('Build complete!');
        });
    });
});